import styled from 'styled-components';
import type { FlexBoxProps } from 'interfaces/components/flexBox';

const FlexBox = styled.div<FlexBoxProps>`
  display: flex;
  justify-content: ${(props) => props.justifyContent};
  align-items: ${(props) => props.alignItems};
  width: ${(props) => props.width};
  flex-direction: ${(props) => props.direction};
  row-gap: ${(props) => props.rowGap || 0}px;
  column-gap: ${(props) => props.columnGap || 0}px;
  flex-wrap: ${(props) => props.wrap};
`;

export default FlexBox;
