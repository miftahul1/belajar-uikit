import { Button } from 'antd';
import styled, { StyledComponent } from 'styled-components';
import { getButtonSize, getButtonIconSize } from 'utils/style/getButtonSize';
import {
  getButtonVariant,
  getButtonIconVariant,
} from 'utils/style/getButtonVariant';

import type {
  StyledButtonProps,
  StyledButtonIconProps,
} from 'interfaces/components/button';

export const StyledButton: StyledComponent<
  any,
  any,
  StyledButtonProps
> = styled(Button)<StyledButtonProps>`
  ${getButtonVariant};
  ${getButtonSize};
  height: fit-content;
  display: flex;
  align-items: center;
  gap: 8px;
  justify-content: center;
`;

export const StyledSuffix = styled.span`
  margin-left: 8px;
`;

export const StyledPrefix = styled.span`
  margin-right: 8px;
`;

export const ButtonIcon = styled.button<StyledButtonIconProps>`
  ${(props) => getButtonIconVariant(props.variant)}
  ${(props) => getButtonIconSize(props.size)}
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0px;
  cursor: pointer;
`;
