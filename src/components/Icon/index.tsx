import React from 'react';
import IcomoonReact from 'icomoon-react';
import colors from 'constants/colors';

import type { IconProps } from 'interfaces/components/icon';

import iconSet from './selection.json';
import { ColorIndex } from 'interfaces/colors';

const IconLocal: React.FC<IconProps> = ({ name, className }) => {
  if (!name && typeof name !== 'string') return null;

  const directoryPath = '/assets/';
  const src = [directoryPath, name, '.svg'].join('');

  return <img src={src} alt="icon" className={className} />;
};

const Icon: React.FC<IconProps> = ({
  size = '24px',
  type = 'external',
  className,
  name,
  color = '',
}) => {
  switch (type) {
    case 'local':
      return <IconLocal type={type} name={name} className={className} />;
    case 'external':
      return (
        <IcomoonReact
          className={className}
          iconSet={iconSet}
          color={colors[color as ColorIndex] || color}
          size={size}
          icon={name}
          type={type}
        />
      );
    default:
      return <IconLocal type={type} name={name} className={className} />;
  }
};

export default Icon;
