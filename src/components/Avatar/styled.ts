import { Avatar } from 'antd';
import styled, { css } from 'styled-components';

import { colors } from 'constants/index';
import { avatarSize } from 'constants/avatarSize';

import type { AvatarProps } from 'interfaces/components/avatar';
import type { AvatarIndex } from 'interfaces/constants/avatarSize';
import getBoxShadow from 'utils/style/getBoxShadow';

const getAvatarSizeValue = ({ size }: { size: AvatarIndex }) => css`
  width: ${avatarSize[size].size};
  height: ${avatarSize[size].size};
  border: ${avatarSize[size].border || 'none'};
  box-shadow: ${avatarSize[size].withShadow ? getBoxShadow('default') : 'none'};
`;

export const StyledAvatar = styled(Avatar)<AvatarProps>`
  ${getAvatarSizeValue}
  color: ${colors.p500};
  background-color: ${colors.p50};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const AvatarContainer = styled.div`
  display: inline-block;
`;
