import { ButtonSize } from 'interfaces/buttonSize';

export const buttonSize: ButtonSize = {
  xs: {
    fontStyle: 'captionBold',
    padding: '4px 12px',
  },
  sm: {
    fontStyle: 'bodyDefault14Bold',
    padding: '5px 12px',
  },
  md: {
    fontStyle: 'bodyDefault16Bold',
    padding: '9px 16px',
  },
  lg: {
    fontStyle: 'bodyDefault16Bold',
    padding: '10px 16px',
  },
  xl: {
    fontStyle: 'bodyDefault16Bold',
    padding: '14px 20px',
  },
};
