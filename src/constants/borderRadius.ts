import BorderRadius from '../interfaces/borderRadius';

const borderRadius: BorderRadius = {
  circle: '50%',
  sharp: '0',
  default: '4px',
  label: '16px',
};

export default borderRadius;
