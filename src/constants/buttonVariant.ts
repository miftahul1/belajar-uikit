import ButtonVariant from 'interfaces/buttonVariant';
import { boxShadowType } from 'interfaces/getBoxShadow';
import getBoxShadow from 'utils/style/getBoxShadow';
import boxShadow from './boxShadow';
import colors from './colors';

const buttonVariant: ButtonVariant = {
  primary: {
    state: {
      loading: {
        color: colors.p400,
        fontColor: colors.n0,
        border: `1px solid ${colors.p400}`,
      },
      disabled: {
        color: colors.n50,
        fontColor: colors.n200,
        border: `1px solid ${colors.n50}`,
      },
    },
    default: {
      color: colors.p500,
      fontColor: colors.n0,
      border: `1px solid ${colors.p500}`,
    },
    hover: {
      color: colors.p700,
      fontColor: colors.n0,
      border: `1px solid ${colors.p700}`,
    },
    pressed: {
      color: colors.p500,
      fontColor: colors.n0,
      border: `1px solid ${colors.p500}`,
      boxShadow: `0px 0px 0px 6px ${colors.p500}25`,
    },
  },
  secondary: {
    state: {
      loading: {
        color: colors.n50,
        fontColor: colors.n900,
        border: `1px solid ${colors.n300}`,
      },
      disabled: {
        color: colors.n50,
        fontColor: colors.n200,
        border: `1px solid ${colors.n200}`,
      },
    },
    default: {
      color: colors.n0,
      border: `1px solid ${colors.n300}`,
      fontColor: colors.n900,
    },
    hover: {
      color: colors.n50,
      border: `1px solid ${colors.n300}`,
      fontColor: colors.n900,
    },
    pressed: {
      color: colors.n0,
      border: `1px solid ${colors.n300}`,
      fontColor: colors.n900,
      boxShadow: `0px 0px 0px 6px ${colors.p500}25`,
    },
  },
  tertiary: {
    state: {
      loading: {
        color: colors.p50,
        fontColor: colors.p500,
        border: `1px solid ${colors.p500}`,
      },
      disabled: {
        color: colors.n50,
        fontColor: colors.n200,
        border: `1px solid ${colors.n200}`,
      },
    },
    default: {
      color: colors.n0,
      border: `1px solid ${colors.p500}`,
      fontColor: colors.p500,
    },
    hover: {
      color: colors.p50,
      border: `1px solid ${colors.p500}`,
      fontColor: colors.p500,
    },
    pressed: {
      color: colors.n0,
      border: `1px solid ${colors.p500}`,
      fontColor: colors.p500,
      boxShadow: getBoxShadow(boxShadow.primary.key as boxShadowType),
    },
  },
  danger: {
    state: {
      loading: {
        color: colors.ar400,
        fontColor: colors.n0,
        border: `1px solid ${colors.ar400}`,
      },
      disabled: {
        color: colors.n50,
        fontColor: colors.n200,
        border: `1px solid ${colors.n50}`,
      },
    },
    default: {
      color: colors.ar500,
      fontColor: colors.n0,
      border: `1px solid ${colors.ar500}`,
    },
    hover: {
      color: colors.ar700,
      fontColor: colors.n0,
      border: `1px solid ${colors.ar700}`,
    },
    pressed: {
      color: colors.ar500,
      fontColor: colors.n0,
      border: `1px solid ${colors.ar500}`,
      boxShadow: `0px 0px 0px 6px ${colors.ar500}25`,
    },
  },
  naked: {
    state: {
      loading: {
        color: colors.n0,
        fontColor: colors.p500,
        border: 'none',
      },
      disabled: {
        color: colors.n0,
        fontColor: colors.n200,
        border: 'none',
      },
    },
    default: {
      color: colors.n0,
      fontColor: colors.p500,
      border: 'none',
    },
    hover: {
      color: colors.p50,
      fontColor: colors.p500,
      border: 'none',
    },
    pressed: {
      color: colors.p100,
      fontColor: colors.p500,
      border: 'none',
    },
  },
  link: {
    state: {
      loading: {
        color: colors.p50,
        fontColor: colors.p500,
      },
      disabled: {
        color: colors.n50,
        fontColor: colors.n200,
      },
    },
    default: {
      color: colors.n0,
      fontColor: colors.p500,
    },
    hover: {
      color: colors.p50,
      fontColor: colors.p500,
    },
    pressed: {
      color: colors.n0,
      fontColor: colors.p500,
    },
  },
};

export default buttonVariant;
