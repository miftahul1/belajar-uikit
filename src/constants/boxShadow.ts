import BoxShadow from 'interfaces/boxShadow';
import colors from './colors';

const boxShadow: BoxShadow = {
  default: {
    key: 'default',
    value: `0px 0px 8px ${colors.n200}`,
  },
  card: {
    key: 'card',
    value: `0px 6px 16px -4px ${colors.n300}`,
  },
  tooltip: {
    key: 'tooltip',
    value: `4px 24px ${colors.n300}`,
  },
  overlay: {
    key: 'overlay',
    value: `4px 32px 8px ${colors.n500}`,
  },
  primary: {
    key: 'primary',
    value: `0px 0px 0px 6px ${colors.p100}`,
  },
  input: {
    key: 'input',
    value: `0 0 0 2px ${colors.p100}`,
  },
};

export default boxShadow;
