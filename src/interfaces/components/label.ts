import { TagProps as AntdLabelProps } from 'antd';

import type { LabelSizeIndex } from 'interfaces/constants/labelSize';
import type { LabelVariantIndex } from 'interfaces/constants/labelVariant';

export interface LabelProps extends AntdLabelProps {
  size?: LabelSizeIndex;
  type?: LabelVariantIndex;
}
