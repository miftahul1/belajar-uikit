import { ColorIndex } from 'interfaces/colors';
import { IcomoonType } from 'interfaces/icomoonType';

export type LocalIcon =
  | 'google'
  | 'mail'
  | 'notification'
  | 'plus'
  | 'OnBoarding1'
  | 'google'
  | 'belajarId'
  | 'greduLogo'
  | 'IconAdd'
  | 'emptyState1'
  | 'emptyState2'
  | 'emptyStateError1'
  | 'emptyStateError2'
  | 'emptyStateSuccess1'
  | 'emptyStateSuccess2'
  | 'emptyStateLoading'
  | 'emptyStateWelcome'
  | 'gredu-class-info'
  | 'gredu-feature-email'
  | 'gredu-email-outline'
  | 'gredu-whatsapp-outline'
  | 'gredu-call-outline'
  | 'subjectIcon'
  | 'taskIcon';

export interface IconProps {
  color?: ColorIndex | string;
  size?: string | number;
  name: LocalIcon | IcomoonType;
  className?: string;
  type?: 'local' | 'external';
}
