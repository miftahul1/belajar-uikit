import type { AvatarIndex } from 'interfaces/constants/avatarSize';
import type { AvatarProps as AntdAvatarProps } from 'antd';

export interface AvatarProps extends Omit<AntdAvatarProps, 'size'> {
  size: AvatarIndex;
}
