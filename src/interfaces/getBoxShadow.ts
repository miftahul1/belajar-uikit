export type boxShadowType =
  | 'card'
  | 'tooltip'
  | 'overlay'
  | 'primary'
  | 'input'
  | 'default';
