import type { Color } from 'interfaces/colors';

export type labelVariantCollection = {
  color: Color;
  border?: string;
  fontColor?: Color;
};

export type LabelVariant = {
  default: labelVariantCollection;
  warning: labelVariantCollection;
  disclaimer: labelVariantCollection;
  info: labelVariantCollection;
  error: labelVariantCollection;
  primary: labelVariantCollection;
  success: labelVariantCollection;
};

export type LabelVariantIndex =
  | 'default'
  | 'warning'
  | 'disclaimer'
  | 'info'
  | 'error'
  | 'primary'
  | 'success';
