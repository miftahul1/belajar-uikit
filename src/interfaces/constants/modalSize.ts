import type { TypoIndex } from 'interfaces/typo';

export type ModalSizeIndex = 'sm' | 'md' | 'classSetting' | 'xl';

export type ModalSizeCollection = {
  headerPadding: string;
  headerTitle: TypoIndex;
  bodyWidth: string;
  footerPadding: string;
};

export interface ModalSize {
  sm: ModalSizeCollection;
  md: ModalSizeCollection;
  xl: ModalSizeCollection;
  classSetting: ModalSizeCollection;
}
