import { Color } from 'interfaces/colors';

export type BadgeState = {
  default: Color;
  warning: Color;
  disclaimer: Color;
  info: Color;
  error: Color;
  primaryStudent: Color;
};

export type BadgeStateIndex =
  | 'default'
  | 'warning'
  | 'disclaimer'
  | 'info'
  | 'error'
  | 'primaryStudent';
