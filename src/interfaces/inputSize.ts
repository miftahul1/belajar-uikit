export type InputSizeType = 'sm' | 'md' | 'lg';

type InputSizeCollection = {
  height: string;
};

export interface InputSize {
  sm: InputSizeCollection;
  md: InputSizeCollection;
  lg: InputSizeCollection;
}
