type TypoCollection = {
  size: string;
  lineHeight: string;
  weight: number;
  letterSpacing?: string;
};

type Typo = {
  heading1: TypoCollection;
  heading2: TypoCollection;
  title1: TypoCollection;
  title1Bold: TypoCollection;
  title2: TypoCollection;
  title2Bold: TypoCollection;
  title3: TypoCollection;
  title3Bold: TypoCollection;
  title4: TypoCollection;
  title4SemiBold: TypoCollection;
  title4Bold: TypoCollection;
  bodyDefault16: TypoCollection;
  bodyDefault16Longer: TypoCollection;
  bodyDefault16SemiBold: TypoCollection;
  bodyDefault16Bold: TypoCollection;
  bodyDefault14: TypoCollection;
  bodyDefault14Longer: TypoCollection;
  bodyDefault14SemiBold: TypoCollection;
  bodyDefault14Bold: TypoCollection;
  caption: TypoCollection;
  captionBold: TypoCollection;
  captionSmall: TypoCollection;
  captionSmallBold: TypoCollection;
  uppercaseCaption: TypoCollection;
  uppercaseCaptionSmall: TypoCollection;
};

export type TypoIndex =
  | 'heading1'
  | 'heading2'
  | 'title1'
  | 'title1Bold'
  | 'title2'
  | 'title2Bold'
  | 'title3'
  | 'title3Bold'
  | 'title4'
  | 'title4SemiBold'
  | 'title4Bold'
  | 'bodyDefault16'
  | 'bodyDefault16Longer'
  | 'bodyDefault16SemiBold'
  | 'bodyDefault16Bold'
  | 'bodyDefault14'
  | 'bodyDefault14Longer'
  | 'bodyDefault14SemiBold'
  | 'bodyDefault14Bold'
  | 'caption'
  | 'captionBold'
  | 'captionSmall'
  | 'captionSmallBold'
  | 'uppercaseCaption'
  | 'uppercaseCaptionSmall';

export default Typo;
