import { css } from 'styled-components';

import { typo } from 'constants/index';

import type { TypoIndex } from 'interfaces/typo';

const getFontStyle = (fontType: TypoIndex) => {
  return css`
    font-weight: ${typo[fontType].weight} !important;
    font-size: ${typo[fontType].size} !important;
    line-height: ${typo[fontType].lineHeight};
    letter-spacing: ${typo[fontType].letterSpacing};
  `;
};

export default getFontStyle;
