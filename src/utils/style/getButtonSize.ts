import { buttonSize } from 'constants/buttonSize';
import { ButtonSizeIndex } from 'interfaces/buttonSize';
import { css } from 'styled-components';
import { TypoIndex } from 'interfaces/typo';
import getFontStyle from './getFontStyle';

export const getButtonSize = ({ size }: { size: ButtonSizeIndex | string }) => {
  const { fontStyle, padding }: { fontStyle: TypoIndex; padding: string } =
    buttonSize[size as ButtonSizeIndex];
  return css`
    ${getFontStyle(fontStyle)}
    padding: ${padding};
  `;
};

export const getButtonIconSize = (size: ButtonSizeIndex = 'sm') => {
  const getSize = () => {
    switch (size) {
      case 'xs':
        return css`
          width: 24px;
          height: 24px;
        `;
      case 'sm':
        return css`
          width: 32px;
          height: 32px;
        `;
      case 'md':
        return css`
          width: 44px;
          height: 40px;
        `;
      case 'lg':
        return css`
          width: 48px;
          height: 44px;
        `;
      case 'xl':
        return css`
          width: 56px;
          height: 52px;
        `;
      default:
        return css`
          width: 32px;
          height: 32px;
        `;
    }
  };

  return css`
    ${getSize()}
  `;
};
