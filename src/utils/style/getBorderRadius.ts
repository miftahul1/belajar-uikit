import { borderRadius } from 'constants/index';
import { BorderRadiusType } from 'interfaces/getBorderRadius';
import { css } from 'styled-components';

const getBorderRadius = (type: BorderRadiusType = 'default') => {
  return css`
    border-radius: ${borderRadius[type as BorderRadiusType]};
  `;
};

export default getBorderRadius;
