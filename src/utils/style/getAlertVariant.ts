import { css } from 'styled-components';

import { alertVariant } from 'constants/index';
import type { AlertType } from 'interfaces/components/alert';
import getBorderRadius from 'utils/style/getBorderRadius';

const getAlertVariant = ({ variant }: { variant: AlertType }) => {
  return css`
    background-color: ${alertVariant[variant].backgroundColor};
    color: ${alertVariant[variant].textColor};
    border: solid 1px ${alertVariant[variant].borderColor};
    ${getBorderRadius()}
  `;
};

export default getAlertVariant;
