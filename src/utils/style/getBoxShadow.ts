import boxShadow from 'constants/boxShadow';
import { boxShadowType } from 'interfaces/getBoxShadow';

const getBoxShadow = (type: boxShadowType) => {
  switch (type) {
    case boxShadow.card.key:
      return boxShadow.card.value;
    case boxShadow.tooltip.key:
      return boxShadow.tooltip.value;
    case boxShadow.overlay.key:
      return boxShadow.overlay.value;
    case boxShadow.primary.key:
      return boxShadow.primary.value;
    case boxShadow.input.key:
      return boxShadow.input.value;

    default:
      return boxShadow.default.value;
  }
};

export default getBoxShadow;
